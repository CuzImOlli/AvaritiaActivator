package de.imolli.avaritaactivator;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainWindow extends Application {

    private double xOffset = 0;
    private double yOffset = 0;
    private static Stage stage;
    private static CurseDialog dialog;
    private static String modpackLocation = "";
    private static Controller controller;
    private static boolean paused = false;
    private static FinishDialog finishDialog;

    @Override
    public void start(final Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Avaritia Activator");
        primaryStage.setScene(new Scene(root, 500, 300));
        primaryStage.setResizable(false);

        primaryStage.initStyle(StageStyle.UNDECORATED);

        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>()

        {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });

        primaryStage.show();

        stage = primaryStage;

    }

    public static void main(String[] args) {

        dialog = new CurseDialog();

        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }

    public static CurseDialog getDialog() {
        return dialog;
    }

    public static void setModpackLocation(String modpackLocation) {
        MainWindow.modpackLocation = modpackLocation;
    }

    public static String getModpackLocation() {
        return modpackLocation;
    }

    public static Controller getController() {
        return controller;
    }

    public static void setController(Controller controller) {
        MainWindow.controller = controller;
    }

    public static boolean isPaused() {
        return paused;
    }

    public static FinishDialog getFinishDialog() {
        return finishDialog;
    }

    public static void setFinishDialog(FinishDialog finishDialog) {
        MainWindow.finishDialog = finishDialog;
    }
}
