package de.imolli.avaritaactivator;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;

import java.io.File;

public class DialogController {

    @FXML
    Label info;


    @FXML
    ChoiceBox choiceBox;

    @FXML
    TextField field;

    @FXML
    protected void onChoosePressed() {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(null);

        if (selectedDirectory == null) {
            field.setText("");
            MainWindow.getDialog().setCurselocation("");
        } else {
            field.setText(selectedDirectory.getPath());
            MainWindow.getDialog().setCurselocation(selectedDirectory.getPath());
        }

        if (!MainWindow.getDialog().getCurselocation().equalsIgnoreCase("")) {
            choiceBox.setItems(FXCollections.observableArrayList(MainWindow.getDialog().getModpacks()));
        }

    }

    @FXML
    protected void onChooseModpackPressed() {

        if (choiceBox.getValue() != null) {
            MainWindow.setModpackLocation(MainWindow.getDialog().getCurselocation() + "\\" + choiceBox.getValue());
            MainWindow.getDialog().getStage().close();
            MainWindow.getController().updateTextField();
        } else {
            MainWindow.getDialog().getStage().close();
        }
    }

    @FXML
    protected void onExitPressed() {

        MainWindow.getDialog().getStage().close();

    }

    @FXML
    public void initialize() {
        System.out.println("second");

        CurseDialog.setController(this);

        System.out.println(MainWindow.getDialog().getCurselocation());

        if (!MainWindow.getDialog().getCurselocation().equalsIgnoreCase("")) {

            field.setText(MainWindow.getDialog().getCurselocation());
            choiceBox.setItems(FXCollections.observableArrayList(MainWindow.getDialog().getModpacks()));

        }
    }

    public void updateItems() {
        if (!MainWindow.getDialog().getCurselocation().equalsIgnoreCase("")) {
            choiceBox.setItems(FXCollections.observableArrayList(MainWindow.getDialog().getModpacks()));
        }
    }

    public void showInfo() {
        info.setVisible(true);
    }

}
