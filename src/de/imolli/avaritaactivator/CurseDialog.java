package de.imolli.avaritaactivator;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;

public class CurseDialog {

    private double xOffset = 0;
    private double yOffset = 0;
    private Stage stage;
    private String curselocation = "";
    private static DialogController controller;

    public void launch() {

        try {
            curselocation = findCurseLocation();
            if (curselocation.equalsIgnoreCase("")) getController().showInfo();

            this.stage = new Stage();

            Parent root = FXMLLoader.load(getClass().getResource("dialog.fxml"));
            stage.setTitle("Curse Modpack Finder");
            stage.setScene(new Scene(root, 350, 250));
            stage.setResizable(false);
            stage.setAlwaysOnTop(true);

            stage.initStyle(StageStyle.UNDECORATED);

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });

            stage.show();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private String findCurseLocation() {

        File file = new File("E:/Program Files/Curse/Minecraft/Instances");
        if (file.exists()) return file.getPath();

        File file2 = new File("C:/Program Files/Curse/Minecraft/Instances");
        if (file2.exists()) return file2.getPath();

        File file3 = new File("Z:/Program Files/Curse/Minecraft/Instances");
        if (file3.exists()) return file3.getPath();

        return "";
    }


    public ArrayList<String> getModpacks() {

        File file = new File(curselocation);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                File file = new File(current, name);

                if (file.isDirectory()) {
                    return isAvaritiaInstalled(file);
                } else return false;

            }
        });

        System.out.println(Arrays.toString(directories));

        return new ArrayList<String>(Arrays.asList(directories));

    }

    @SuppressWarnings("ConstantConditions")
    public Boolean isAvaritiaInstalled(File dir) {

        if (!dir.exists()) return false;

        return dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.equalsIgnoreCase("config") && new File(dir, name).isDirectory() && new File(dir, name).list(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.equalsIgnoreCase("Avaritia.cfg");
                    }
                }).length != 0;
            }
        }).length != 0;

    }

    public Stage getStage() {
        return stage;
    }

    public String getCurselocation() {
        return curselocation;
    }

    public void setCurselocation(String curselocation) {
        this.curselocation = curselocation;
    }

    public static DialogController getController() {
        return controller;
    }

    public static void setController(DialogController controller) {
        CurseDialog.controller = controller;
    }
}
