package de.imolli.avaritaactivator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Controller {

    @FXML
    Button activate;

    @FXML
    Button choose;

    @FXML
    TextField field;

    @FXML
    protected void onChooseButtonPressed() {

        if (MainWindow.isPaused()) return;

        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(null);

        if (selectedDirectory == null) {
            field.setText("");
        } else {
            field.setText(selectedDirectory.getPath());
        }
    }

    @FXML
    protected void onTextChanged() {

        System.out.println("DEBUG!");

        MainWindow.setModpackLocation(field.getText());

    }

    @FXML
    protected void onActivatePressed() {
        if (MainWindow.isPaused()) return;

        if (MainWindow.getModpackLocation().equalsIgnoreCase("")) {
            return;
        }

        if (MainWindow.getDialog().isAvaritiaInstalled(new File(MainWindow.getModpackLocation()))) {

            Path path = Paths.get(MainWindow.getModpackLocation() + "\\config\\Avaritia.cfg");
            Charset charset = StandardCharsets.UTF_8;

            try {

                String content = new String(Files.readAllBytes(path), charset);
                content = content.replaceAll("B:\"Crafting Only\"=false", "B:\"Crafting Only\"=true");
                Files.write(path, content.getBytes(charset));

                FinishDialog.setMessage("Successfully turned Avaritia on in Modpack " + MainWindow.getModpackLocation() + "!");

                FinishDialog finishDialog = new FinishDialog();
                finishDialog.launch();

                MainWindow.setFinishDialog(finishDialog);

                MainWindow.getStage().close();

            } catch (IOException e) {
                e.printStackTrace();

                FinishDialog.setMessage("Failed to turn on Avaritia! Please try again!");

                FinishDialog finishDialog = new FinishDialog();
                finishDialog.launch();

                MainWindow.setFinishDialog(finishDialog);

                MainWindow.getStage().close();

            }
        } else {
            FinishDialog.setMessage("Failed to turn on Avaritia. Modpack does not contain Avaritia!");

            FinishDialog finishDialog = new FinishDialog();

            finishDialog.launch();

            MainWindow.setFinishDialog(finishDialog);
        }
    }

    @FXML
    protected void onExitPressed() {
        System.exit(0);
    }

    @FXML
    protected void onCursePressed() {
        if (MainWindow.isPaused()) return;

        MainWindow.getDialog().launch();

    }

    @FXML
    public void initialize() {
        MainWindow.setController(this);
    }

    public void updateTextField() {
        field.setText(MainWindow.getModpackLocation());
    }

}
