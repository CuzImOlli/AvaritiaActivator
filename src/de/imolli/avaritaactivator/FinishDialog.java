package de.imolli.avaritaactivator;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class FinishDialog {

    private static double xOffset = 0;
    private static double yOffset = 0;
    private static Stage stage;
    private static String message;

    public void launch() {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("finish.fxml"));

            stage = new Stage();

            stage.setTitle("Finish!");
            stage.setScene(new Scene(root, 300, 170));
            stage.setResizable(false);
            stage.setAlwaysOnTop(true);

            stage.initStyle(StageStyle.UNDECORATED);

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });

            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String message) {
        FinishDialog.message = message;
    }

    public Stage getStage() {
        return stage;
    }

}
