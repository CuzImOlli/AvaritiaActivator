package de.imolli.avaritaactivator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class FinishDialogController {

    @FXML
    Button close;

    @FXML
    Label info;

    @FXML
    protected void onClosePressed() {
        MainWindow.getFinishDialog().getStage().close();
    }

    @FXML
    public void initialize() {
        info.setText(FinishDialog.getMessage());
    }

}
